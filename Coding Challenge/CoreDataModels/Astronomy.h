//
//  Astronomy.h
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Astronomy : NSManagedObject

@property (nonatomic, retain) NSString * sunrise_;
@property (nonatomic, retain) NSString * sunset_;
@property (nonatomic, retain) NSManagedObject *channel_;

@end
