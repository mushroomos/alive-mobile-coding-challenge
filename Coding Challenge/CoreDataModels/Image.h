//
//  Image.h
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Channel;

@interface Image : NSManagedObject

@property (nonatomic, retain) NSString * title_;
@property (nonatomic, retain) NSString * width_;
@property (nonatomic, retain) NSString * height_;
@property (nonatomic, retain) NSString * link_;
@property (nonatomic, retain) NSString * url_;
@property (nonatomic, retain) Channel *channel_;

@end
