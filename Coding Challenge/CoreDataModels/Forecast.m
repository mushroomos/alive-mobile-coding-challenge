//
//  Forecast.m
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import "Forecast.h"
#import "Item.h"


@implementation Forecast

@dynamic code_;
@dynamic date_;
@dynamic day_;
@dynamic high_;
@dynamic low_;
@dynamic text_;
@dynamic item_;

@end
