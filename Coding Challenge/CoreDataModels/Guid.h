//
//  Guid.h
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item;

@interface Guid : NSManagedObject

@property (nonatomic, retain) NSString * isPermaLink_;
@property (nonatomic, retain) NSString * content_;
@property (nonatomic, retain) Item *item_;

@end
