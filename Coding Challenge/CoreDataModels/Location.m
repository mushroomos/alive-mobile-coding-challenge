//
//  Location.m
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import "Location.h"


@implementation Location

@dynamic city_;
@dynamic country_;
@dynamic region_;
@dynamic channel_;

@end
