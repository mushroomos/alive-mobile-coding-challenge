//
//  Channel.h
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Astronomy, Atmosphere, Item, Location, Units, Wind;

@interface Channel : NSManagedObject

@property (nonatomic, retain) NSString * title_;
@property (nonatomic, retain) NSString * description_;
@property (nonatomic, retain) NSString * link_;
@property (nonatomic, retain) NSString * language_;
@property (nonatomic, retain) NSString * lastBuildDate_;
@property (nonatomic, retain) NSString * ttl_;
@property (nonatomic, retain) NSManagedObject *results_;
@property (nonatomic, retain) Location *location_;
@property (nonatomic, retain) Astronomy *astronomy_;
@property (nonatomic, retain) Atmosphere *atmosphere_;
@property (nonatomic, retain) Wind *wind_;
@property (nonatomic, retain) Units *units_;
@property (nonatomic, retain) NSManagedObject *image_;
@property (nonatomic, retain) Item *item_;

@end
