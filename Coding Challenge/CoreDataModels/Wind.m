//
//  Wind.m
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import "Wind.h"


@implementation Wind

@dynamic chill_;
@dynamic direction_;
@dynamic speed_;
@dynamic channel_;

@end
