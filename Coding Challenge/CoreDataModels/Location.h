//
//  Location.h
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Location : NSManagedObject

@property (nonatomic, retain) NSString * city_;
@property (nonatomic, retain) NSString * country_;
@property (nonatomic, retain) NSString * region_;
@property (nonatomic, retain) NSManagedObject *channel_;

@end
