//
//  Atmosphere.m
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import "Atmosphere.h"


@implementation Atmosphere

@dynamic humidity_;
@dynamic pressure_;
@dynamic rising_;
@dynamic visibility_;
@dynamic channel_;

@end
