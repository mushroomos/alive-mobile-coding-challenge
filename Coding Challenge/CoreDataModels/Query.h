//
//  Query.h
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Results;

@interface Query : NSManagedObject

@property (nonatomic, retain) NSNumber * count_;
@property (nonatomic, retain) NSString * created_;
@property (nonatomic, retain) NSString * lang_;
@property (nonatomic, retain) Results *results_;

@end
