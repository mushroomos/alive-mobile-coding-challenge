//
//  Wind.h
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Wind : NSManagedObject

@property (nonatomic, retain) NSString * chill_;
@property (nonatomic, retain) NSString * direction_;
@property (nonatomic, retain) NSString * speed_;
@property (nonatomic, retain) NSManagedObject *channel_;

@end
