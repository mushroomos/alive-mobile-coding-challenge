//
//  Query.m
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import "Query.h"
#import "Results.h"


@implementation Query

@dynamic count_;
@dynamic created_;
@dynamic lang_;
@dynamic results_;

@end
