//
//  Condition.h
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item;

@interface Condition : NSManagedObject

@property (nonatomic, retain) NSString * code_;
@property (nonatomic, retain) NSString * date_;
@property (nonatomic, retain) NSString * temp_;
@property (nonatomic, retain) NSString * text_;
@property (nonatomic, retain) Item *item_;

@end
