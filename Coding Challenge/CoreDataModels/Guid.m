//
//  Guid.m
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import "Guid.h"
#import "Item.h"


@implementation Guid

@dynamic isPermaLink_;
@dynamic content_;
@dynamic item_;

@end
