//
//  Astronomy.m
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import "Astronomy.h"


@implementation Astronomy

@dynamic sunrise_;
@dynamic sunset_;
@dynamic channel_;

@end
