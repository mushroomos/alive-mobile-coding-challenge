//
//  Image.m
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import "Image.h"
#import "Channel.h"


@implementation Image

@dynamic title_;
@dynamic width_;
@dynamic height_;
@dynamic link_;
@dynamic url_;
@dynamic channel_;

@end
