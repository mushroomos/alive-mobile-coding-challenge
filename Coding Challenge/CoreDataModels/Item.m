//
//  Item.m
//  Coding Challenge
//
//  Created by IOS dev on 21/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import "Item.h"
#import "Channel.h"
#import "Condition.h"
#import "Forecast.h"
#import "Guid.h"


@implementation Item

@dynamic description_;
@dynamic lat_;
@dynamic link_;
@dynamic long_;
@dynamic pubDate_;
@dynamic title_;
@dynamic channel_;
@dynamic condition_;
@dynamic forecast_;
@dynamic guid_;

-(void)addForecast_Object:(Forecast *)value{
//    NSMutableOrderedSet *tempSet = [NSMutableOrderedSet orderedSetWithOrderedSet:self.forecast_];
//    [tempSet addObject:value];
//    self.forecast_ = tempSet;
    NSMutableOrderedSet *tempSet = [self mutableOrderedSetValueForKey:@"forecast_"];
    [tempSet addObject:value];
}

@end
