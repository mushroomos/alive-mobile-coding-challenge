//
//  Forecast.h
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item;

@interface Forecast : NSManagedObject

@property (nonatomic, retain) NSString * code_;
@property (nonatomic, retain) NSString * date_;
@property (nonatomic, retain) NSString * day_;
@property (nonatomic, retain) NSString * high_;
@property (nonatomic, retain) NSString * low_;
@property (nonatomic, retain) NSString * text_;
@property (nonatomic, retain) Item *item_;

@end
