//
//  Units.m
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import "Units.h"


@implementation Units

@dynamic distance_;
@dynamic pressure_;
@dynamic speed_;
@dynamic temperature_;
@dynamic channel_;

@end
