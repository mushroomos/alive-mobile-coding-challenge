//
//  Channel.m
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import "Channel.h"
#import "Astronomy.h"
#import "Atmosphere.h"
#import "Item.h"
#import "Location.h"
#import "Units.h"
#import "Wind.h"


@implementation Channel

@dynamic title_;
@dynamic description_;
@dynamic link_;
@dynamic language_;
@dynamic lastBuildDate_;
@dynamic ttl_;
@dynamic results_;
@dynamic location_;
@dynamic astronomy_;
@dynamic atmosphere_;
@dynamic wind_;
@dynamic units_;
@dynamic image_;
@dynamic item_;

@end
