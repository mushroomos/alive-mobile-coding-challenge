//
//  Item.h
//  Coding Challenge
//
//  Created by IOS dev on 21/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Channel, Condition, Forecast, Guid;

@interface Item : NSManagedObject

@property (nonatomic, retain) NSString * description_;
@property (nonatomic, retain) NSString * lat_;
@property (nonatomic, retain) NSString * link_;
@property (nonatomic, retain) NSString * long_;
@property (nonatomic, retain) NSString * pubDate_;
@property (nonatomic, retain) NSString * title_;
@property (nonatomic, retain) Channel *channel_;
@property (nonatomic, retain) Condition *condition_;
@property (nonatomic, retain) NSOrderedSet *forecast_;
@property (nonatomic, retain) Guid *guid_;
@end

@interface Item (CoreDataGeneratedAccessors)

- (void)insertObject:(Forecast *)value inForecast_AtIndex:(NSUInteger)idx;
- (void)removeObjectFromForecast_AtIndex:(NSUInteger)idx;
- (void)insertForecast_:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeForecast_AtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInForecast_AtIndex:(NSUInteger)idx withObject:(Forecast *)value;
- (void)replaceForecast_AtIndexes:(NSIndexSet *)indexes withForecast_:(NSArray *)values;
- (void)addForecast_Object:(Forecast *)value;
- (void)removeForecast_Object:(Forecast *)value;
- (void)addForecast_:(NSOrderedSet *)values;
- (void)removeForecast_:(NSOrderedSet *)values;
@end
