//
//  ACCAppDelegate.h
//  Coding Challenge
//
//  Created by Matt Tonkin on 11/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
// coredatabooks

#import <UIKit/UIKit.h>

@interface ACCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

/*
 * Define Core Data essentials
 */
@property (nonatomic, strong, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end
