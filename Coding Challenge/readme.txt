The app does:
1. Let user to choose location (Sydney or Mosman)
2. After user selects the location, the app will show the forecast for the next few days.
3. The app saves all data to core data with multiple Entities that will best represent the data
4. Each time the weather is checked, the results will be saved to local database in Sqlite database(since XML databse is not avaliable to iOS)
5. The app support lightweight migrate of coredata to support the changes of the data.
6. The app is Universal