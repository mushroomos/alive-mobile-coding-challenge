//
//  ForecastViewController.h
//  Coding Challenge
//
//  Created by IOS dev on 21/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForecastViewController : UIViewController

@property (nonatomic, copy) NSString *jsonUrl;

@end
