//
//  MainViewController.m
//  Coding Challenge
//
//  Created by IOS dev on 19/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import "MainViewController.h"
#import "ForecastViewController.h"

@interface MainViewController ()
<UITextFieldDelegate, UIPickerViewDataSource,UIPickerViewDelegate>
@property (nonatomic, strong) IBOutlet UITextField *locationField;
@property (nonatomic, strong) IBOutlet UIPickerView *pickerView;
@property (nonatomic, strong) IBOutlet UIToolbar *toolBar;
@property (nonatomic, strong) NSDictionary *locationDictionary;

/*
 * All UI init goes here
 */
- (void)initUI;
/*
 * All Data init goes here
 */
- (void)initData;
/*
 * Resign keyboard
 */
- (IBAction)resignKeyboard:(id)sender;
/*
 * Push ForecastViewController in
 */
- (IBAction)goForecastView:(id)sender;
@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initData];
    [self initUI];
}

- (void)initUI{
    self.title = @"Main";
    self.locationField.text = [[self.locationDictionary allKeys] objectAtIndex:0];
}

- (void)initData{
    self.locationDictionary = @{@"Sydney":KSydneyJsonUrl,
                                @"Mosman":KMosmanJsonUrl};
}
#pragma mark - Instance Methods
- (IBAction)resignKeyboard:(id)sender{
    [self.locationField resignFirstResponder];
}
- (IBAction)goForecastView:(id)sender{
    ForecastViewController *controller = [[ForecastViewController alloc] init];
    controller.jsonUrl = [self.locationDictionary objectForKey:self.locationField.text];
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - UIPickerViewDataSource & UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [[self.locationDictionary allKeys] count];
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [[self.locationDictionary allKeys] objectAtIndex:row];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.locationField.text = [[self.locationDictionary allKeys] objectAtIndex:row];
}
#pragma mark - UITextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [textField setInputView:self.pickerView];
    [textField setInputAccessoryView:self.toolBar];
}
@end
