//
//  ForecastViewController.m
//  Coding Challenge
//
//  Created by IOS dev on 21/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import "ForecastViewController.h"
#import "JsonHelper.h"

#define KCellHeight 150


@interface ForecastViewController ()
<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *forecastArray;
/*
 * Asynchronously fetch Json Data From the URL
 */
- (void)fetchJsonDataFromUrl:(NSString *)jsonUrl;
/*
 * Asynchronously parsing Json Data to News Data Structure
 * GCD is used here
 */
- (void)asyncParsingJsonData:(NSData *)data;
/*
 * Create an alertview with given message
 */
- (void)alertWithMessage:(NSString *)message;
/*
 * All UI init goes here
 */
- (void)initUI;
/*
 * All Data init goes here
 */
- (void)initData;
@end

@implementation ForecastViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initData];
    [self initUI];
}
#pragma mark - Instance Methods
- (void)fetchJsonDataFromUrl:(NSString *)jsonUrl{
    NSURLRequest *jsonRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:jsonUrl]];
    [NSURLConnection sendAsynchronousRequest:jsonRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (!error) {
                                   [self asyncParsingJsonData:data];
                               }
                               else{
                                   [self alertWithMessage:[NSString stringWithFormat:@"%@",error]];
                               }
                           }];
}
- (void)asyncParsingJsonData:(NSData *)data{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //*** Load Json Data
        self.forecastArray = [JsonHelper saveToCoreDataWithJsonData:data];
        dispatch_async(dispatch_get_main_queue(), ^{
            //*** Do anything about UI here
            [self.tableView reloadData];
        });
    });
}
- (void)alertWithMessage:(NSString *)message{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error!" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}
- (void)initUI{
    self.title = @"Forecast";
}
- (void)initData{
    [self fetchJsonDataFromUrl:self.jsonUrl];
}
#pragma mark - UITableView DataSource
#pragma mark - UITableView DataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return KCellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.forecastArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = indexPath.row;
    NSDictionary *currentForecast = [self.forecastArray objectAtIndex:row];
    static NSString *CellIdentifier = @"CellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.detailTextLabel.numberOfLines = 0;
    }
    cell.textLabel.text = [currentForecast objectForKey:@"date"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"code: %@\nday: %@\nhigh: %@\nlow: %@\ntext: %@",
                                 [currentForecast objectForKey:@"code"],
                                 [currentForecast objectForKey:@"day"],
                                 [currentForecast objectForKey:@"high"],
                                 [currentForecast objectForKey:@"low"],
                                 [currentForecast objectForKey:@"text"]];
    
    return cell;
}

#pragma mark - UITableView Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}
@end
