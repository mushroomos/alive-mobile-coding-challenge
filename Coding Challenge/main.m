//
//  main.m
//  Coding Challenge
//
//  Created by Matt Tonkin on 11/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ACCAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([ACCAppDelegate class]));
	}
}
