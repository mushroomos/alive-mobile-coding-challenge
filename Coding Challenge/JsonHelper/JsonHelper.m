//
//  JsonHelper.m
//  Coding Challenge
//
//  Created by IOS dev on 20/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import "JsonHelper.h"
#import "ACCAppDelegate.h"
#import "Query.h"
#import "Results.h"
#import "Channel.h"
#import "Location.h"
#import "Units.h"
#import "Wind.h"
#import "Atmosphere.h"
#import "Astronomy.h"
#import "Image.h"
#import "Item.h"
#import "Condition.h"
#import "Forecast.h"
#import "Guid.h"

@implementation JsonHelper

+(NSArray *)saveToCoreDataWithJsonData:(NSData *)jsonData{
    // *** Start to init all essential variables
    ACCAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = appDelegate.managedObjectContext;

    // *** Parsing Json ***
    NSError *error = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    if (error){
        NSLog(@"JSONObjectWithData error: %@", error);
    }
    // *** Core data saving
    
    /*
     * Query
     */
    NSDictionary *queryDictionary = [dictionary objectForKey:@"query"];
    Query *query = [NSEntityDescription insertNewObjectForEntityForName:@"Query"
                                                  inManagedObjectContext:managedObjectContext];
    query.lang_ = [queryDictionary objectForKey:@"lang"];
    int countInt = [(NSString *)[queryDictionary objectForKey:@"count"] intValue];
    query.count_ = [NSNumber numberWithInt:countInt];
    query.created_ = [queryDictionary objectForKey:@"created"];
    
    /*
     * Results
     */
    NSDictionary *resultsDictionary = [queryDictionary objectForKey:@"results"];
    Results *results = [NSEntityDescription insertNewObjectForEntityForName:@"Results"
                                                  inManagedObjectContext:managedObjectContext];
    results.query_ = query;
    query.results_ = results;
    /*
     * Channel
     */
    NSDictionary *channelDictionary = [resultsDictionary objectForKey:@"channel"];
    Channel *channel = [NSEntityDescription insertNewObjectForEntityForName:@"Channel"
                                                     inManagedObjectContext:managedObjectContext];
    channel.description_ = [channelDictionary objectForKey:@"description"];
    channel.language_ = [channelDictionary objectForKey:@"language"];
    channel.link_ = [channelDictionary objectForKey:@"link"];
    channel.title_ = [channelDictionary objectForKey:@"title"];
    channel.ttl_ = [channelDictionary objectForKey:@"ttl"];
    channel.lastBuildDate_ = [channelDictionary objectForKey:@"lastBuildDate"];
    
    channel.results_ = results;
    results.channel_ = channel;
    
    /*
     * Location
     */
    NSDictionary *locationDictionary = [channelDictionary objectForKey:@"location"];
    Location *location = [NSEntityDescription insertNewObjectForEntityForName:@"Location"
                                                     inManagedObjectContext:managedObjectContext];
    location.city_ = [locationDictionary objectForKey:@"city"];
    location.country_ = [locationDictionary objectForKey:@"country"];
    location.region_ = [locationDictionary objectForKey:@"region"];
    
    location.channel_ = channel;
    channel.location_ = location;
    
    /*
     * Units
     */
    NSDictionary *unitsDictionary = [channelDictionary objectForKey:@"units"];
    Units *units = [NSEntityDescription insertNewObjectForEntityForName:@"Units"
                                                 inManagedObjectContext:managedObjectContext];
    units.distance_ = [unitsDictionary objectForKey:@"distance"];
    units.pressure_ = [unitsDictionary objectForKey:@"pressure"];
    units.speed_ = [unitsDictionary objectForKey:@"speed"];
    units.temperature_ = [unitsDictionary objectForKey:@"temperature"];
    
    units.channel_ = channel;
    channel.units_ = units;
    /*
     * Wind
     */
    NSDictionary *windDictionary = [channelDictionary objectForKey:@"wind"];
    Wind *wind = [NSEntityDescription insertNewObjectForEntityForName:@"Wind"
                                               inManagedObjectContext:managedObjectContext];
    wind.chill_ = [windDictionary objectForKey:@"chill"];
    wind.direction_ = [windDictionary objectForKey:@"direction"];
    wind.speed_ = [windDictionary objectForKey:@"speed"];
    
    wind.channel_ = channel;
    channel.wind_ = wind;
    /*
     * Atmosphere
     */
    NSDictionary *atmosphereDictionary = [channelDictionary objectForKey:@"atmosphere"];
    Atmosphere *atmosphere = [NSEntityDescription insertNewObjectForEntityForName:@"Atmosphere"
                                                           inManagedObjectContext:managedObjectContext];
    atmosphere.humidity_ = [atmosphereDictionary objectForKey:@"humidity"];
    atmosphere.pressure_ = [atmosphereDictionary objectForKey:@"pressure"];
    atmosphere.rising_ = [atmosphereDictionary objectForKey:@"rising"];
    atmosphere.visibility_ = [atmosphereDictionary objectForKey:@"visibility"];
    
    atmosphere.channel_ = channel;
    channel.atmosphere_ = atmosphere;
    /*
     * Astronomy
     */
    NSDictionary *astronomyDictionary = [channelDictionary objectForKey:@"astronomy"];
    NSDateFormatter * astronomyformatter = [[NSDateFormatter alloc] init];
    [astronomyformatter setAMSymbol:@"am"];
    [astronomyformatter setPMSymbol:@"pm"];
    [astronomyformatter setDateFormat:@"m:ss a"];
    
    Astronomy *astronomy = [NSEntityDescription insertNewObjectForEntityForName:@"Astronomy"
                                                           inManagedObjectContext:managedObjectContext];
    astronomy.sunrise_ = [astronomyDictionary objectForKey:@"sunrise"];
    astronomy.sunset_ = [astronomyDictionary objectForKey:@"sunset"];
    
    astronomy.channel_ = channel;
    channel.astronomy_ = astronomy;
    /*
     * Image
     */
    NSDictionary *imageDictionary = [channelDictionary objectForKey:@"image"];
    Image *image = [NSEntityDescription insertNewObjectForEntityForName:@"Image"
                                                 inManagedObjectContext:managedObjectContext];
    image.title_ = [imageDictionary objectForKey:@"title"];
    image.width_ = [imageDictionary objectForKey:@"width"];
    image.height_ = [imageDictionary objectForKey:@"height"];
    image.link_ = [imageDictionary objectForKey:@"link"];
    image.url_ = [imageDictionary objectForKey:@"url"];
    
    image.channel_ = channel;
    channel.image_  = image;
    /*
     * Item
     */
    NSDictionary *itemDictionary = [channelDictionary objectForKey:@"item"];
    Item *item = [NSEntityDescription insertNewObjectForEntityForName:@"Item"
                                               inManagedObjectContext:managedObjectContext];
    item.description_ = [itemDictionary objectForKey:@"description"];
    item.lat_ = [itemDictionary objectForKey:@"lat"];
    item.long_ = [itemDictionary objectForKey:@"long"];
    item.link_ = [itemDictionary objectForKey:@"link"];
    item.pubDate_ = [itemDictionary objectForKey:@"pubDate"];
    item.title_ = [itemDictionary objectForKey:@"title"];
    
    item.channel_ = channel;
    channel.item_ = item;
    
    /*
     * Condition
     */
    NSDictionary *conditionDictionary = [itemDictionary objectForKey:@"condition"];
    Condition *condition = [NSEntityDescription insertNewObjectForEntityForName:@"Condition"
                                               inManagedObjectContext:managedObjectContext];
    condition.code_ = [conditionDictionary objectForKey:@"code"];
    condition.date_ = [conditionDictionary objectForKey:@"date"];
    condition.temp_ = [conditionDictionary objectForKey:@"temp"];
    condition.text_ = [conditionDictionary objectForKey:@"text"];
    
    condition.item_ = item;
    item.condition_ = condition;
    /*
     * Forecast
     */
    NSArray *forecastArray = [itemDictionary objectForKey:@"forecast"];
    //NSMutableArray *mArray = [NSMutableArray array];
    for (NSDictionary *currentForecastDictionary in forecastArray){
        Forecast *forecast = [NSEntityDescription insertNewObjectForEntityForName:@"Forecast"
                                                             inManagedObjectContext:managedObjectContext];
        forecast.code_ = [currentForecastDictionary objectForKey:@"code"];
        forecast.date_ = [currentForecastDictionary objectForKey:@"date"];
        forecast.day_ = [currentForecastDictionary objectForKey:@"day"];
        forecast.high_ = [currentForecastDictionary objectForKey:@"high"];
        forecast.low_ = [currentForecastDictionary objectForKey:@"low"];
        forecast.text_ = [currentForecastDictionary objectForKey:@"text"];
        
        forecast.item_ = item;
        [item addForecast_Object:forecast];
    }
    /*
     * Guid
     */
    NSDictionary *guidDictionary = [itemDictionary objectForKey:@"guid"];
    Guid *guid = [NSEntityDescription insertNewObjectForEntityForName:@"Guid"
                                                         inManagedObjectContext:managedObjectContext];
    guid.content_ = [guidDictionary objectForKey:@"content"];
    guid.isPermaLink_ = [guidDictionary objectForKey:@"isPermaLink"];
    
    guid.item_ = item;
    item.guid_ = guid;
    // *** End of function ***
    NSError *coreError;
    if (![managedObjectContext save:&coreError]) {
        NSLog(@"Core data save Error: %@", [coreError localizedDescription]);
    }
    return forecastArray;
}

@end
