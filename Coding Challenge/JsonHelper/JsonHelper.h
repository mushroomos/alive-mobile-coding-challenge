//
//  JsonHelper.h
//  Coding Challenge
//
//  Created by IOS dev on 20/08/2014.
//  Copyright (c) 2014 Alive Mobile Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JsonHelper : NSObject

+(NSArray *)saveToCoreDataWithJsonData:(NSData *)jsonData;

@end
